//
//  LoginViewController.m
//  Panahon
//
//  Created by Hajji on 7/9/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "LoginViewController.h"
#import "ClassManager.h"

@implementation LoginViewController

@synthesize emailTextField, passwordTextField, textScrollView;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome-bg.png"]]];
    [[self navigationController] setNavigationBarHidden:YES];
    [[self navigationController] setToolbarHidden:YES animated:NO];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    self.slideOutAnimationEnabled = YES;
    
    //Initialize Keyboard Dismiss
    [self initHideKeyboard];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        // returnUserDetails Shared instance will call another graph fetch to ensure user details
        // This shared instance also loads the segue Please see this shared instance to navigate
        [[ClassManager sharedInstance] returnUserDetails];
        
        // User is logged in, do work such as go to next view controller.
        //NSLog(@"%@", [FBSDKProfile currentProfile].name);
        //[[ClassManager sharedInstance] registerAPI];
    }else{
        NSLog(@"No FB Token - LoginViewController! --");
    }
}

- (void) viewDidAppear:(BOOL)animated{
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"Hi Am Logged-in 1");
    }else{
        NSLog(@"Not connected! 1");
    }

}

- (IBAction)fbLoginBtnClick:(id)sender
{

    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"%@", result);
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    
                    [[ClassManager sharedInstance] returnUserDetails];
                    
                    //[[ClassManager sharedInstance] registerAPI];
                    
                    // User is logged in, do work such as go to next view controller.
//                    NSLog(@"Hi Am Logged-in 3");
//                    [[NSNotificationCenter defaultCenter] addObserverForName:FBSDKProfileDidChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
//                        //do whatever you want
//                        
//
//                        NSLog(@"HHHHHHH");
//                    }];
                    
                }else{
                    NSLog(@"Not connected! 3");
                }
            }
        }
    }];
}

- (IBAction)loginBtnClick:(id)sender
{
    
    
    NSString *email = [NSString stringWithFormat:@"%@", emailTextField.text];
    NSString *password = [NSString stringWithFormat:@"%@", passwordTextField.text];
    
    if ((![[ClassManager sharedInstance] emailRegEx:email] || [email isEqualToString:@""]) || [password isEqualToString:@""]) {
        [self alertAction:@"Hi, you have entered an invalid E-MAIL and/or PASSWORD!"];
    }else{
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", nil];
        NSLog(@"PASSWORD: %@", details);
        
        [[ClassManager sharedInstance] fetchLoginDetails:details];
    }
    
}

- (void)alertAction:(NSString *)description
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil message:description delegate:Nil cancelButtonTitle:@"Re-try" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)initHideKeyboard
{
    //Initialize Keyboard Dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tap];
    
    emailTextField.delegate = self;
    passwordTextField.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 302) {
        [self loginBtnClick:textField];
        [textField resignFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    int deviceSize = self.view.frame.size.height;
    int position;
    switch (deviceSize) {
        case 667:
            // iPhone 6
            position = 240;
            break;
        case 568:
            // iPhone 5s
            position = 240;
            break;
        case 736:
            // iPhone 6+
            position = 280;
            break;
        default:
            // iPhone 6 Default
            position = 240;
            break;
    }
    CGPoint scrollPoint = CGPointMake(0, textScrollView.frame.size.height - position);
    
    [textScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textScrollView setContentOffset:CGPointZero animated:YES];
}
@end

