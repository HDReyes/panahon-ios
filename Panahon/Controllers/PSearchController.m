//
//  PSearchController.m
//  Panahon
//
//  Created by Brian Andaliza on 8/6/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "PSearchController.h"

@interface PSearchController ()

@end

@implementation PSearchController


- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self initProvinceList];
        [self initSearchView];
    }
    return self;
}

+ (PSearchController *)sharedInstance
{
    static PSearchController *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [PSearchController new];
    });
    
    return _sharedInstance;
}


- (void)setHidden:(BOOL)hidden
{
    if(hidden == YES)
    {
        self.tblCitiesSubView.frame = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }
    
    [super setHidden:hidden];
}

- (void)initProvinceList
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"provincesWithCities" ofType:@"plist"];
    
    provinces = [NSDictionary dictionaryWithContentsOfFile:path];
    provincesKeys = [[provinces allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    citySelected    = @"PH-NCR";
    
    currCities = [[NSMutableArray alloc] initWithArray:[[provinces valueForKey:citySelected] valueForKey:@"cities"]];
}

- (void)initSearchView
{
    self.tblCitiesView              = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.tblCitiesView.delegate     = self;
    self.tblCitiesView.dataSource   = self;
    self.tblCitiesView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    
    self.tblCitiesSubView               = [[UITableView alloc] initWithFrame:CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    
    self.tblCitiesSubView.delegate      = self;
    self.tblCitiesSubView.dataSource    = self;
    self.tblCitiesSubView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    
    self.clipsToBounds = YES;
    
    [self addSubview:self.tblCitiesView];
    [self addSubview:self.tblCitiesSubView];
}

- (void)animateCitiesSubView
{
    [UIView animateWithDuration:0.5 animations:^{
        self.tblCitiesSubView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }];
}

#pragma mark - UITableView Delegates and Datasource

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tblCitiesSubView && indexPath.row > 0)
    {
        return 3;
    }
    else
    {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.tblCitiesSubView)
    {
        return [currCities count];
    }
    else
    {
        return [provinces count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier  = @"Cell";
    static NSString *cellIdentifier2 = @"Cell2";

    UITableViewCell *cell;
    
    if(tableView == self.tblCitiesSubView)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        cell.textLabel.text = [[currCities objectAtIndex:indexPath.row] valueForKey:@"name"];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier2];
        }
        
        cell.textLabel.text = [[provinces valueForKey:[provincesKeys objectAtIndex:indexPath.row]] valueForKey:@"province"];
    }
    
    
    [cell setSeparatorInset:UIEdgeInsetsZero];
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tblCitiesSubView)
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.tblCitiesSubView.frame = CGRectMake(0, - self.frame.size.height, self.frame.size.width, self.frame.size.height);
        }];
    }
    else
    {
        currCities = [[[provinces valueForKey:[provincesKeys objectAtIndex:indexPath.row]] valueForKey:@"cities"] mutableCopy];
        
        NSDictionary *provInfo = @{@"id" : @"0", @"name" : [[provinces valueForKey:[provincesKeys objectAtIndex:indexPath.row]] valueForKey:@"province"]};
        
        [currCities insertObject:provInfo atIndex:0];
        
        [self.tblCitiesSubView reloadData];
        
        [self animateCitiesSubView];
    }
    
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *code;
    
    if(tableView == self.tblCitiesSubView)
    {
        [self.delegate PSearchView:self didSelectACity:[currCities objectAtIndex:indexPath.row]];
    }
    else
    {
        code = [provincesKeys objectAtIndex:indexPath.row];
        
        [self.delegate PSearchView:self didSelectAProvince:[provinces valueForKey:code]  withCode:code];
    }
}

@end
