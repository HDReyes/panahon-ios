//
//  PShareWeatherController.h
//  Panahon
//
//  Created by Brian Andaliza on 8/3/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "PWeatherType.h"
#import "PHttpClient.h"

@interface PShareWeatherController : UIViewController<PHttpClientDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
{
    PWeatherType *selectedWType;
    PHttpClient *weatherClient;
}

// modal
@property (nonatomic) IBOutlet UIView *modalView;
@property (nonatomic) IBOutlet UILabel *confirmLbl;

@property (strong, nonatomic) CLLocationManager *userLocation;
@property (nonatomic) NSDictionary *currArea;

@property (strong, nonatomic) IBOutlet UITextField *gpsTextField;
@property (strong, nonatomic) IBOutlet UITextField *descTextField;
@property (nonatomic) IBOutlet UIView *weatherTypeContainer;
@property (strong, nonatomic) IBOutlet UIButton *shareWeatherBtn;
//@property (nonatomic) IBOutlet UIActivityIndicatorView *sharingIndicator;

- (IBAction)weatherTypePressed:(UITapGestureRecognizer *)wType;
- (IBAction)shareWeatherPressed;
- (IBAction)hideConfModal;
@end
