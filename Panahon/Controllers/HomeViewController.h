//
//  HomeViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "SlideNavigationController.h"
#import "PSettings.h"
#import "PWeatherAPI.h"
#import "CKRadialMenu.h"
#import "PShareWeatherController.h"
#import "PAskWeatherController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface HomeViewController : UIViewController <SlideNavigationControllerDelegate, CLLocationManagerDelegate, PWeatherAPIDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CKRadialMenuDelegate>
{
    PSettings *connManager;
    PWeatherAPI *weatherAPI;
    
    UIStoryboard *main;
    CLLocationManager *locationManager;
    CLGeocoder *geoCoder;
    CLPlacemark *placemark;
    NSDictionary *settings, *weatherInfo, *provinces, *userCity;
    NSArray *provincesKeys;
    NSMutableArray *currCities;
    
    NSString *province, *citySelected, *currSelected, *cityWeather, *userProvince, *userLocCode;
    
    UIAlertView *radialAlert;
}

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

// Community report
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


// ask / share question text
@property (nonatomic) IBOutlet UILabel *questionLbl;
@property (nonatomic) IBOutlet UIButton *commAskBtn;
@property (nonatomic) IBOutlet UIButton *commShareBtn;

// preloader
@property (nonatomic) IBOutlet UIView *preloaderView;
@property (nonatomic) IBOutlet UIActivityIndicatorView *preActivity;
@property (nonatomic) IBOutlet UILabel *preloaderLbl;

@property (nonatomic) IBOutlet UIImageView *currWeatherIcon;
@property (nonatomic) IBOutlet UILabel *currDateTime;
@property (nonatomic) IBOutlet UIView *currWeatherView;
@property (nonatomic) IBOutlet UIView *threeDayView;
@property (nonatomic) IBOutlet UILabel *tempNumLbl;
@property (nonatomic) IBOutlet UILabel *tempDescLbl;
@property (nonatomic) IBOutlet UIButton *pagasaBtn;

@property (weak, nonatomic) IBOutlet UIButton *closeSearchBtn;
@property (weak, nonatomic) IBOutlet UITextField *selCityField;
@property (weak, nonatomic) IBOutlet UIView *citiesMainView;
@property (strong, nonatomic) IBOutlet UIView *radialMenuContView;
@property (strong, nonatomic) IBOutlet UIView *commReportView;
@property (strong, nonatomic) IBOutlet UIView *commAskView;

@property (strong, nonatomic) IBOutlet UITableView *tblCitiesView;
@property (strong, nonatomic) IBOutlet UIImageView *bgImgView;

@property (strong, nonatomic) UITableView *tblCitiesSubView;
@property (strong, nonatomic) CLLocationManager *userLocation;

- (void)changeBgImage:(NSInteger)code;
- (IBAction)hideSearchParams;
@end
