//
//  PagasaViewController.m
//  Panahon
//
//  Created by Hajji on 7/14/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "PagasaViewController.h"

@interface PagasaViewController ()

@end

@implementation PagasaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    animateData = [NSMutableArray new];
    settings    = [PSettings new];
    
    weatherAPI = [PWeatherAPI new];
    weatherAPI.delegate = self;
    
    imgCount   = 0;
    
    progHud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:progHud];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self connectToAPI];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.msatImgView stopAnimating];
}

- (void)connectToAPI
{
    // Set determinate mode
    progHud.mode = MBProgressHUDModeIndeterminate;
    
    progHud.tag = 1;
    progHud.delegate = self;
    progHud.labelText = @"Connecting to API";
    
    [progHud show:YES];
    
    [weatherAPI retrieveMSATImages];
}

- (void)completedTask
{
    NSLog(@"completed");
}

- (UIImage *)rotateImage:(UIImage *)image
{
    UIImage *rotatedImg = [UIImage imageWithCGImage:[image CGImage] scale:1.0 orientation:UIImageOrientationLeft];
    
    return rotatedImg;
}

- (void)checkImages
{
    [self checkMSATFolder];
    
    NSLog(@"%@", pagasaData);
    
    NSOperationQueue *queue = [NSOperationQueue new];
    
    queue.maxConcurrentOperationCount = 4;
    
    NSBlockOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self dispMSATAnimation];
        }];
    }];
    
    for (NSDictionary *info in pagasaData) {
        
        NSString *imgUrl = [info valueForKey:@"image"];
        
        
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                
            UIImage *msatImage = [weatherAPI getMSATImage:[imgUrl lastPathComponent]];
                
            if(msatImage == nil)
            {
                UIImage *msatImage = [weatherAPI downloadMSATImage:imgUrl];
                    
                [weatherAPI saveMSATImage:msatImage filename:[imgUrl lastPathComponent]];
            }
                
            [animateData addObject:[self rotateImage:msatImage]];
        }];
            
        [completionOperation addDependency:operation];
    }
    
    [queue addOperations:completionOperation.dependencies waitUntilFinished:NO];
    
    [queue addOperation:completionOperation];
    
    //NSString *imgURL   = @"http://panahon.tv/api/public/uploads/mtsat/20150811-730.jpg";
    
    
//    UIImage *rawImage = [weatherAPI getMSATImage:[imgURL lastPathComponent]];
//    
//    if(rawImage == nil)
//    {
//        UIImage *rawImage = [weatherAPI downloadMSATImage:imgURL];
//        
//        [weatherAPI saveMSATImage:rawImage filename:[imgURL lastPathComponent]];
//    }
//    
//    UIImage *msatImage = [UIImage imageWithCGImage:[rawImage CGImage] scale:1.0 orientation:UIImageOrientationLeft];
//    
//    self.msatImgView.image = msatImage;
    
    
    
//    progHud.labelText = [NSString stringWithFormat:@"Loading Images"];
//    
//    [pagasaData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
//     {
//         NSString *imgUrl = [obj valueForKey:@"image"];
//         
//         NSLog(@"%@", imgUrl);
//         
//         self.msatImgView.image = [weatherAPI getMSATImage:[imgUrl lastPathComponent]];
//         
//         UIImage *msatImage = [weatherAPI getMSATImage:[imgUrl lastPathComponent]];
//         
//         if(msatImage == nil)
//         {
//             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                 
//                 UIImage *msatImage = [weatherAPI downloadMSATImage:imgUrl];
//                 
//                 dispatch_sync(dispatch_get_main_queue(), ^{
//
//                     [weatherAPI saveMSATImage:msatImage filename:[imgUrl lastPathComponent]];
//                     
//                     [animateData addObject:msatImage];
//                     
//                     [self checkPagasaCount:idx numberOfImages:[pagasaData count]];
//                 });
//             });
//         }else
//         {
//             [animateData addObject:msatImage];
//             
//             [self checkPagasaCount:idx numberOfImages:[pagasaData count]];
//         }
//     }];
}

- (void)dispMSATAnimation
{
    [progHud hide:YES];
    
//    [UIView animateWithDuration:1 delay:5 options:UIViewAnimationOptionRepeat animations:^{
//        
//        NSLog(@"test");
//        //self.msatImgView.image = [animateData objectAtIndex:imgCount];
//    } completion:^(BOOL finished) {
//        imgCount++;
//        
//        if(imgCount == [animateData count]) imgCount = 0;
//        
//        NSLog(@"test2");
//        
//        //[self dispMSATAnimation];
//    }];
}

- (NSString *)checkMSATFolder
{
    NSString *folder = @"MSAT";
    NSString *dataPath = [[settings dataFilePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", folder]];
    
    NSLog(@"%@", dataPath);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil]; //Create folder
    }
    
    return folder;
}


#pragma mark - PWeatherAPI Delegates
- (void)PWeatherAPI:(PWeatherAPI *)weatherAPI didRetrieveMSATImages:(NSArray *)wImages
{
    NSLog(@"%@", wImages);
    
    pagasaData = wImages;
    
    [self checkImages];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}


@end
