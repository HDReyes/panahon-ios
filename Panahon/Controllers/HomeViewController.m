//
//  HomeViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "HomeViewController.h"
#import "LeftMenuViewController.h"

#import "PConfig.h"

@implementation HomeViewController
{
    NSArray *reportDetails;
}

@synthesize refreshControl;

- (void)initProvinceList
{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"provincesWithCities" ofType: @"plist"];
    
    provinces = [NSDictionary dictionaryWithContentsOfFile: path];
    provincesKeys = [[provinces allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    citySelected    = @"PH-NCR";
    
    currCities = [[NSMutableArray alloc] initWithArray:[[provinces valueForKey:citySelected] valueForKey:@"cities"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Loads Navigation Bar
    [self loadNavigationBar];
    
    // Initialize the pull to refesh
    [self initPullToRefresh];
    
    main                    = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    connManager             = [PSettings new];
    weatherAPI              = [PWeatherAPI new];
    
    weatherAPI.delegate = self;
    
    [self initProvinceList];
    
    
    //Get Users's Location
    self.userLocation                 = [[CLLocationManager alloc] init];
    
    self.userLocation.delegate        = self;
    self.userLocation.desiredAccuracy = kCLLocationAccuracyBest;
    self.userLocation.distanceFilter  = kCLDistanceFilterNone;
    
    // for ios8
    if([self.userLocation respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [self.userLocation requestAlwaysAuthorization];
    }
    
    [self.userLocation startUpdatingLocation];
    
    [self animatePreloaderView:YES];
    //Initialize the navigation header image
    
    geoCoder = [CLGeocoder new];
    
    // call the weather api
    [weatherAPI retrieveWeatherByCity:@"all"];
    
    // current weather view
    self.currWeatherView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"infoBG.png"]];
    
    [self initSearchView];
    
    [self dispRadialMenu];
    
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    
    // alert for radial menu
    radialAlert = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
}

- (void)timerTick:(NSTimer *)timer
{
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"EEEE, h:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    self.currDateTime.text = [dateFormatter stringFromDate:now];
}

- (void)loadNavigationBar{
    [[self navigationController] setNavigationBarHidden:NO];
    [[self navigationController] setToolbarHidden:YES animated:NO];
    
    //Set the Logo
    UIImage *navLogoImg = [UIImage imageNamed:@"panahontv-nav-logo.png"];
    UIImageView *navLogo = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, 12, 47, 19)];
    [navLogo setImage:navLogoImg];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar addSubview:navLogo];

}

- (void)hideSearchParams
{
    self.closeSearchBtn.hidden = YES;
    self.citiesMainView.hidden = YES;
    self.tblCitiesSubView.frame= CGRectMake(0, -300, self.tblCitiesView.frame.size.width, self.tblCitiesView.frame.size.height);
}


- (void)initSearchView
{
    self.tblCitiesView.delegate     = self;
    self.tblCitiesView.dataSource   = self;
    self.tblCitiesView.tag          = 550;
    
    // for sub menu
    self.tblCitiesSubView = [[UITableView alloc] initWithFrame:CGRectMake(0, -300, self.tblCitiesView.frame.size.width, self.tblCitiesView.frame.size.height)];
    
    self.tblCitiesSubView.delegate   = self;
    self.tblCitiesSubView.dataSource = self;
    self.tblCitiesSubView.tag        = 551;
    //tblCitiesSubView.hidden     = YES;
    
    [self.citiesMainView addSubview:self.tblCitiesSubView];
}

- (void)dispRadialMenu
{
    // Frame in initWithFrame will be the frame of the initial "center view"
    CKRadialMenu *radialMenu = [[CKRadialMenu alloc] initWithFrame: CGRectMake(100,80,58,58)];
    
    radialMenu.delegate = self;
    radialMenu.distanceBetweenPopouts = 70.0f;
    radialMenu.distanceFromCenter = 70;
    
    radialMenu.centerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"weather-menu-btn.png"]];
    
    // ask
    UIView *askWeather = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
    
    askWeather.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ask-weather-btn.png"]];
    
    // share
    UIView *shareWeather = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
    
    shareWeather.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"share-weather-btn.png"]];
    
    
    [radialMenu addPopoutView:shareWeather withIndentifier:@"Share Weather"];
    [radialMenu addPopoutView:askWeather withIndentifier:@"Ask Weather"];
    
    [self.radialMenuContView addSubview:radialMenu];
}

- (void)displayUserLocation
{
    // Get User's Latitude and Longitude
    CLLocation *currLocation = [self.userLocation location];

    [geoCoder reverseGeocodeLocation:currLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if(error == nil && [placemarks count] > 0)
        {
            placemark = [placemarks lastObject];
//            placemark.locality;
//            placemark.administrativeArea;
            NSLog(@"MY PLACE: - %@ --- %@", placemark.locality, placemark.administrativeArea);
            [self checkAdminAreaExistsAPI];
            
            NSLog(@"MY PLACE View WILL APPEAR: - %@ --- %@", placemark.locality, placemark.administrativeArea);
            NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:placemark.administrativeArea, @"province", placemark.locality, @"city",nil];
            
            [self reloadTableData:locationDetails];
        }
    }];
    
}

- (void)checkAdminAreaExistsAPI
{
    [provinces enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        if([[obj valueForKey:@"province"] isEqualToString:placemark.administrativeArea])
        {
            userLocCode  = key;
            userProvince = [obj valueForKey:@"province"];
            
            NSArray *city = [[obj valueForKey:@"cities"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name == %@", placemark.locality]];
            
            if([city count] > 0) userCity = [city objectAtIndex:0];
            
            self.selCityField.text = [NSString stringWithFormat:@"%@ / %@", userProvince, [userCity valueForKey:@"name"]];
            
            //userCity
            
            NSLog(@"PROVINCE: %@, CITY: %@", userProvince, userCity);
            
        }
    }];
}

- (void)animatePreloaderView:(BOOL)show
{
    if(show)
    {
        self.preloaderView.hidden = NO;
        self.preloaderLbl.text    = @"Checking Internet Connection..";
        [self.preActivity startAnimating];
    }
    else
    {
        self.preloaderView.hidden = YES;
        [self.preActivity stopAnimating];
        
    }
}

- (void)changeBgImage:(NSInteger)code
{
    NSString *bg;
    
    switch (code) {
        
        // clear skies
        case 1:
            bg = @"clear-skies.jpg";
            break;
        
        // partly cloudy skies
        case 2:
            bg = @"cloudy.jpg";
            break;
        
        // partly cloudy to at times cloudy with rain showers
        case 3:
            bg = @"partly-cloudy-rain-showers.jpg";
            break;
        
        // partly cloudy to at times cloudy with thunderstorms
        case 4:
            bg = @"partly-cloudy-thunderstorms.jpg";
            break;
        
        // cloudy skies with rain showers
        case 5:
            bg = @"cloudy-skies-rain-thunders.jpg";
            break;
        
        // light rains
        case 6:
            bg = @"light-rains.jpg";
            break;
        
        // occassional rains
        case 7:
            bg = @"cloudy-skies-rain-thunders.jpg";
            break;
        
        // monsoon rains
        case 8:
            bg = @"monsoon-rains.jpg";
            break;
        
        // rains with gusty wind
        case 10:
            bg = @"partly-cloudy-rain-showers.jpg";
            break;
        
        // stormy
        case 11:
            bg = @"stormy.jpg";
            break;
            
        default:
            bg = @"welcome-bg.png";
            break;
    }

    [UIView transitionWithView:self.bgImgView
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.bgImgView.image = [UIImage imageNamed:bg];
                    } completion:nil];
}

- (void)updateWeatherInfo
{
    weatherInfo = [[NSDictionary alloc] initWithContentsOfFile:[[connManager dataFilePath] stringByAppendingPathComponent:@"weatherCities.plist"]];
    
    cityWeather = [weatherInfo valueForKey:citySelected];
    
    // change bg image
    NSInteger code = [[cityWeather valueForKey:@"weather_code"] intValue];
    
    [self changeBgImage:code];
    
    self.currWeatherIcon.image  = [UIImage imageNamed:[NSString stringWithFormat:@"wicon-%ld.png", code]];
    
    // temperature
    self.tempNumLbl.text  = [NSString stringWithFormat:@"%@ - %@ºC", [cityWeather valueForKey:@"temp_low"], [cityWeather valueForKey:@"temp_high"]];
    
    // description
    [self.tempDescLbl setAdjustsFontSizeToFitWidth:YES];
    
    self.tempDescLbl.text = [cityWeather valueForKey:@"weather_description"];
    
    [self threeDayWeatherInfo];
    
    [self animatePreloaderView:NO];
    
    [self showWeatherResults];
}

- (void)showWeatherResults
{
    // add animations later
    
    [UIView animateWithDuration:2.0 animations:^{
        self.currWeatherView.hidden = NO;
        self.threeDayView.hidden    = NO;
        self.pagasaBtn.hidden       = NO;
        self.selCityField.hidden    = NO;
        self.currDateTime.hidden    = NO;
    }];
}

- (void)threeDayWeatherInfo
{
    // clear all subviews
    [[self.threeDayView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray *advForecasts = [cityWeather valueForKey:@"advanced_forecasts"];
    
    int b        = ((self.threeDayView.frame.size.width - (37 * 3)) / 2);
    int currLocX = 0;

    
    
    // three-day weather
    for (int a = 0; a <= 2; a++)
    {
        NSDictionary *nextInfo = [advForecasts objectAtIndex:a];
        
        NSString *dateString = [[[nextInfo valueForKey:@"name_date"] substringToIndex:3] uppercaseString];
        
        UIImageView *wiconView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 2, 30, 30)];

        wiconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"wicon-%@.png", [nextInfo valueForKey:@"weather_code"]]];
        UILabel *nextDayLbl         = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 37, 20)];
        nextDayLbl.text             = dateString;
        nextDayLbl.textAlignment    = NSTextAlignmentCenter;
        nextDayLbl.textColor        = [UIColor whiteColor];
        nextDayLbl.font             = [UIFont systemFontOfSize:12.0f];
        
        UIView *nextDayView = [UIView new];
        
        if(a > 0)
        {
            currLocX = (37 * a) + (b * a);
        }
        
        nextDayView.frame   = CGRectMake(currLocX, 0, 37, 50);
        
        
        nextDayView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"infoBG.png"]];
        
        [nextDayView addSubview:wiconView];
        [nextDayView addSubview:nextDayLbl];
        [self.threeDayView addSubview:nextDayView];
    }
}

- (void)animateCitiesSubView
{
    [UIView animateWithDuration:0.5 animations:^{
        self.tblCitiesSubView.frame = CGRectMake(0, 0, self.tblCitiesView.frame.size.width, self.tblCitiesView.frame.size.height);
    }];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

#pragma mark - CLLocation Delegates
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.userLocation stopUpdatingLocation];
    
    NSLog(@"%@", locations);
    
    [self displayUserLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

#pragma mark - PWeatherAPI Delegates
- (void)PWeatherAPI:(PWeatherAPI *)weatherAPI didRetrieveWeatherByCity:(NSDictionary *)weatherData
{
    // write weather of each ph code
    [weatherData writeToFile:[[connManager dataFilePath] stringByAppendingPathComponent:@"weatherCities.plist"] atomically:YES];
    
    self.commReportView.hidden = NO;
    self.radialMenuContView.hidden = NO;
    
    [self updateWeatherInfo];
}

#pragma mark - UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 551 && indexPath.row > 0)
    {
        return 3;
    }
    else
    {
        return 0;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tblCitiesSubView)
    {
        return [currCities count];
    }
    else if(tableView == self.tblCitiesView)
    {
        return [provincesKeys count];
    }else{
        return [reportDetails count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    static NSString *cellIdentifier2 = @"Cell2";
    static NSString *sharedWeatherCell = @"sharedWeatherCell";
    
    UITableViewCell *cell;
    
    if(tableView == self.tblCitiesSubView)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
        cell.textLabel.text  = [[currCities objectAtIndex:indexPath.row] valueForKey:@"name"];
    }
    else if(tableView == self.tblCitiesView)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier2];
        }
        
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
        cell.textLabel.text = [[provinces valueForKey:[provincesKeys objectAtIndex:indexPath.row]] valueForKey:@"province"];
    }else{
    
    
        NSLog(@"Table Here!");
        cell = [tableView dequeueReusableCellWithIdentifier:sharedWeatherCell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sharedWeatherCell];
        }
        
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        [cell.textLabel setTextColor:[UIColor blackColor]];
        
        
        NSString *name = [[reportDetails objectAtIndex:indexPath.row] objectForKey:@"firstname"];
        NSString *weatherType = [[reportDetails objectAtIndex:indexPath.row] objectForKey:@"weather_type"];
        NSString *city = [[reportDetails objectAtIndex:indexPath.row] objectForKey:@"city"];
        NSString *date_shared = [[reportDetails objectAtIndex:indexPath.row] objectForKey:@"date_shared"];
        
        
        // Adjusts the icon position on smaller iOS device
        //    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
        //        icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 8, 30, 30)];
        //    }else{
        //    }
        //    icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 16, 14, 14)];
        
        cell.backgroundColor = [UIColor clearColor];
        
        NSString *facebook_id = [[reportDetails objectAtIndex:indexPath.row] objectForKey:@"facebook_id"];
        
        if (![facebook_id isEqualToString:@"0"] || ![facebook_id isEqualToString:@""]) {
            
            
            
                // Build Facebook URL for profile picture
                NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large&width=40&height=40", facebook_id]];
                NSLog(@"URL: %@", url);
                
                UIImageView *profile     = (UIImageView *)[cell viewWithTag:101];
                //[profile setImage:[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]]];
            
                [profile setShowActivityIndicatorView:YES];
                [profile setIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
                profile.contentMode = UIViewContentModeScaleAspectFill;
                [profile sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
            
                [[profile layer] setCornerRadius:20.0f];
                [profile layer].masksToBounds = YES;
            
            
                UIImageView *weatherTypeImage = (UIImageView *)[cell viewWithTag:102];
                [weatherTypeImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"wcode%@.png", weatherType]]];
                [[weatherTypeImage layer] setCornerRadius:20.0f];
                [weatherTypeImage layer].masksToBounds = YES;
            
            
            
        }
        
        
        
        UILabel *username = (UILabel *)[cell viewWithTag:103];
        username.text = name;
        
        UILabel *location = (UILabel *)[cell viewWithTag:104];
        location.text = city;
        
        UILabel *date = (UILabel *)[cell viewWithTag:100];
        date.text = [self timeStrip:date_shared];
        //date.text = date_shared;
        
        // Set up the cell...
        //[cell addSubview: [icon initWithImage:[UIImage imageNamed:[menu[indexPath.row] objectForKey:@"icon"]]]];
        //cell.textLabel.text = [NSString stringWithFormat:@"%@-%@-%@", name, weatherType, city];
        //cell.imageView.image = [UIImage imageNamed:[menu[indexPath.row] objectForKey:@"icon"]];
        //cell.textLabel.numberOfLines = 1;
        
        
    }
    
    return cell;
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 551)
    {
        if(indexPath.row == 0) 
        {
            [UIView animateWithDuration:0.5 animations:^{
                self.tblCitiesSubView.frame = CGRectMake(0, -300, self.tblCitiesView.frame.size.width, self.tblCitiesView.frame.size.height);
            }];
        }
        else
        {
            citySelected = currSelected;
            
            [self updateWeatherInfo];
            
            [self hideSearchParams];
        }
    }
    else
    {
        currSelected = [provincesKeys objectAtIndex:indexPath.row];
        
        currCities = [[[provinces valueForKey:currSelected] valueForKey:@"cities"] mutableCopy];
        
        NSDictionary *provInfo = @{@"id": @"0", @"name" : [[provinces valueForKey:currSelected] valueForKey:@"province"]};
        
        [currCities insertObject:provInfo atIndex:0];
        
        [self.tblCitiesSubView reloadData];
        
        [self animateCitiesSubView];
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 551)
    {
        [self updateWeatherInfo];

        userCity = [currCities objectAtIndex:indexPath.row];
        
        self.selCityField.text = [NSString stringWithFormat:@"%@ / %@", userProvince, [userCity valueForKey:@"name"]];
        
        NSString *strProvince;
        NSString *cityStr;
        
        if (![userProvince isEqualToString:@""]) {
            strProvince = userProvince;
            if ([userCity count] > 0) {
                cityStr = [userCity valueForKey:@"name"];
                
                if ([strProvince isEqualToString:cityStr]) {
                    cityStr = @"";
                }
                
                NSLog(@"PROVINCE: %@ ===== CITY: %@", strProvince, cityStr);
                NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:strProvince, @"province", cityStr, @"city",nil];
                [self reloadTableData:locationDetails];
            }
        }
    }
    else
    {
        userProvince = [[provinces valueForKey:currSelected] valueForKey:@"province"];
        userLocCode = currSelected;
    }
    
    NSLog(@"LOCATION 1: %@, LOCATION 2 %@", userProvince, userCity);
    
    
    

    
    
//    NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:userProvince, @"province", [userCity valueForKey:@"name"], @"city",nil];
//    [self reloadTableData:locationDetails];
}

#pragma mark - UITextfield Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.closeSearchBtn.hidden = NO;
    self.citiesMainView.hidden = NO;
    
    return NO;
}

#pragma mark - CKRadialMenu Delegates
-(void)radialMenu:(CKRadialMenu *)radialMenu didSelectPopoutWithIndentifier:(NSString *)identifier
{
    if([identifier isEqualToString:@"Share Weather"])
    {
        if(placemark == nil)
        {
            radialAlert.title = @"Error!";
            radialAlert.message = @"You need to activate your location services settings first.";
            
            [radialAlert show];
        }
        else
        {
            [self performSegueWithIdentifier:@"pushToShareWeatherForm" sender:self];
        }
    }
    else if ([identifier isEqualToString:@"Ask Weather"])
    {
        [self performSegueWithIdentifier:@"pushToAskWeatherForm" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"pushToShareWeatherForm"])
    {
        PShareWeatherController *shareWeather = segue.destinationViewController;
        
        shareWeather.userLocation = self.userLocation;
        
        shareWeather.currArea = [NSDictionary dictionaryWithObjectsAndKeys:
                                 placemark.administrativeArea, @"province",
                                 placemark.locality, @"city", nil];
        
        
    }
    else if ([segue.identifier isEqualToString:@"pushToAskWeatherForm"])
    {
        PAskWeatherController *askWeather = segue.destinationViewController;
        
        askWeather.userCity = userCity;
        askWeather.userProvince = [provinces valueForKey:userLocCode];
        askWeather.userLocCode  = userLocCode;
    }
}


// COMMUNITY CODING STARTS HERE

- (void)initPullToRefresh
{
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(pullDownReload) forControlEvents:UIControlEventValueChanged];
    //[refreshControl performSelectorOnMainThread:@selector(reloadTableData:) withObject:locationDetails waitUntilDone:TRUE];
    
    [self.tableView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:TRUE];
    
//    NSLog(@"MY PLACE View WILL APPEAR: - %@ --- %@", placemark.locality, placemark.administrativeArea);
//    NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:@"Calabarzon", @"province", @"Carmona City", @"city", @"10", @"limit",nil];
//    
//    [self reloadTableData:locationDetails];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:TRUE];
    
//    NSLog(@"MY PLACE View WILL APPEAR: - %@ --- %@", placemark.locality, placemark.administrativeArea);
//    NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:@"Calabarzon", @"province", @"Carmona City", @"city", @"10", @"limit",nil];
//    
//    [self reloadTableData:locationDetails];
}

- (void)reloadTableData:(NSDictionary *)locationDetails
{
    NSDictionary *details = locationDetails;
    if ([locationDetails count] == 0) {
        details = [[NSDictionary alloc] initWithObjectsAndKeys:@"Metro Manila", @"province", @"", @"city", nil];
    }
    
    // This is an Async Call to fetch new data from server
    [self fetchCommunityReport:details addCall:^(BOOL newData, NSString *response, int errcode, NSArray *data){
        if (newData) {
            for (id userInfo in reportDetails){
                NSLog(@"TRUE: %@", userInfo);
                [self.tableView reloadData];
            }
        }else{
            [self.tableView reloadData];
        }
    }];
}

- (void)pullDownReload
{
    //NSDictionary *locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:@"Metro Manila", @"province", @"Makati City", @"city", @"10", @"limit",nil];
    NSDictionary *locationDetails;
    if([userProvince isEqualToString:[userCity valueForKey:@"name"]]){
        locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:userProvince, @"province", @"", @"city",nil];
    }else{
        locationDetails = [[NSDictionary alloc] initWithObjectsAndKeys:userProvince, @"province", [userCity valueForKey:@"name"], @"city",nil];
    }
    
    // This is an Async Call to fetch new data from server
    [self fetchCommunityReport:locationDetails addCall:^(BOOL newData, NSString *response, int errcode, NSArray *data){
        if (newData) {
            for (id userInfo in reportDetails){
                NSLog(@"TRUE: %@", userInfo);
                [self.tableView reloadData];
                [refreshControl endRefreshing];
            }
        }else{
            [self.tableView reloadData];
            [refreshControl endRefreshing];
        }
    }];
    
}

- (void)fetchCommunityReport:(NSDictionary *)locationDetails addCall:(void (^)(BOOL newData, NSString *response, int errcode, NSArray *data))callback
{
    NSMutableDictionary *details = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[locationDetails objectForKey:@"province"], @"province", [locationDetails objectForKey:@"city"], @"city", @"10", @"limit", nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"http://fbappbox.com/panahontv-api/index.php/api_call/community_report/" parameters:details success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        BOOL status        = [[responseObject objectForKey:@"status"] boolValue];
        NSString *reponse  = [responseObject objectForKey:@"response"];
        int errcode        = [[responseObject objectForKey:@"errcode"] intValue];
        
        if (status) {
            reportDetails       = [responseObject objectForKey:@"data"];
        }else{
            reportDetails = [[NSArray alloc] init];
        }
        
        NSLog(@"%@", responseObject);
        callback(status,reponse,errcode,reportDetails);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        
    }];
    
}



/************/
/***** TABLEVIEW STARTS HERE *******/
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return [reportDetails count];
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
//    view.backgroundColor = [UIColor clearColor];
//    return view;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 1;
//}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    
//    
//    
//    return cell;
//}

- (NSString *)timeStrip:(NSString *)date
{
    //Format Current Date
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    NSDate *now = [NSDate date];
    //    NSString *strDate = [dateFormatter stringFromDate:date];
    //    return strDate;
    
    
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateConv =[dateFormatter dateFromString:date];
    NSLog(@"DATE: %@", dateConv);
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setDateFormat:@"hh:mm a"];
    NSString *time=[dfTime stringFromDate:dateConv];
    return time;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"%ld", (long)indexPath.row);
//    
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //Sets the cell BG according with the Table BG
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    // change background color of selected cell
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    [myBackView setBackgroundColor:[UIColor colorWithWhite:1.000 alpha:0.000]];
    [myBackView setAlpha:0];
    [myBackView setOpaque:NO];
    cell.selectedBackgroundView = myBackView;
    
}

//- (UIImageView *)returnFacebookProfileImage:(NSString *)facebook_id
//{
//    FBSDKProfilePictureView *pictureView =[[FBSDKProfilePictureView alloc] init];
//    [pictureView setProfileID:facebook_id];
//    [pictureView setPictureMode:FBSDKProfilePictureModeSquare];
//    [pictureView layer].cornerRadius = 35.0f;
//    [pictureView layer].masksToBounds = YES;
//    
//    return pictureView;
//}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    NSLog(@"Clear Cache!");
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDisk];
}

@end
