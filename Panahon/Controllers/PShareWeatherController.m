//
//  PShareWeatherController.m
//  Panahon
//
//  Created by Brian Andaliza on 8/3/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import "PShareWeatherController.h"
#import "PConfig.h"
#import "ClassManager.h"

@interface PShareWeatherController ()

@end

@implementation PShareWeatherController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNavigation];
    // Initialize Tap Gesture to dismiss keyboard
    [self initHideKeyboard];
    
    weatherClient           = [PHttpClient new];
    weatherClient.delegate  = self;

}
- (void)initNavigation{
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"profile-page-yellow-bg.png"]]];
    [[self navigationController] setNavigationBarHidden:NO];
    [[self navigationController] setToolbarHidden:YES animated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.gpsTextField.text = [NSString stringWithFormat:@"%@ / %@", [self.currArea valueForKey:@"province"], [self.currArea valueForKey:@"city"]];
    
    self.modalView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
}

- (void)shareWeatherPressed
{
    self.shareWeatherBtn.enabled = NO;
    
    NSDictionary *profileInfo = [[ClassManager sharedInstance] profileDetails];
    
    CLLocation *loc = [self.userLocation location];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              PANAHON_API_KEY, @"api_key",
                              [self.currArea valueForKey:@"province"], @"province",
                              [self.currArea valueForKey:@"city"], @"city",
                              self.descTextField.text, @"description",
                              selectedWType.type, @"weather_type",
                              [NSString stringWithFormat:@"%f", loc.coordinate.longitude], @"longitude",
                              [NSString stringWithFormat:@"%f", loc.coordinate.latitude], @"latitude",
                              [profileInfo valueForKey:@"userid"], @"userid",
                            nil];
    
    if(selectedWType == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Select first a sticker and try again!" delegate:self cancelButtonTitle:@"Noted" otherButtonTitles:nil];
    
        [alert show];
        
        self.shareWeatherBtn.enabled = YES;
    }else
    {
        [weatherClient connectToURL:PANAHON_SHARE_WEATHER_URL withData:postData postType:@"post"];
    }
    
}

- (void)initHideKeyboard
{
    //Initialize Keyboard Dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tap];
    
//    emailTextField.delegate = self;
//    passwordTextField.delegate = self;
}

- (void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)weatherTypePressed:(UITapGestureRecognizer *)wType
{
    [self resetAllWeatherTypes];
    
    selectedWType = (PWeatherType *)[wType view];
    
    selectedWType.enabled = YES;
}

- (void)resetAllWeatherTypes
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class == %@", [PWeatherType class]];
    
    NSArray *result = [[self.weatherTypeContainer subviews] filteredArrayUsingPredicate:predicate];
    
    for (PWeatherType *weatherView in result)
    {
        weatherView.enabled = NO;
    }
}

- (void)dispConfModal:(NSDictionary *)data
{
    self.modalView.hidden = NO;
    
    self.confirmLbl.text = [NSString stringWithFormat:@"%@", [data valueForKey:@"response"]];
}

- (void)hideConfModal
{
    self.modalView.hidden = YES;
}

#pragma mark - PHttpClient Delegates
- (void)PHttpClient:(PHttpClient *)httpClient connectionDataWasRetrieved:(id)data
{
    self.shareWeatherBtn.enabled = YES;
    
    [self dispConfModal:data];
}

@end
