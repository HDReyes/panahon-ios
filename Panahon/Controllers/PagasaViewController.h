//
//  PagasaViewController.h
//  Panahon
//
//  Created by Hajji on 7/14/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

#import "MBProgressHUD.h"
#import "PHttpClient.h"
#import "PWeatherAPI.h"
#import "PSettings.h"

@interface PagasaViewController : UIViewController<MBProgressHUDDelegate, PWeatherAPIDelegate>
{
    PSettings *settings;
    MBProgressHUD *progHud;
    PHttpClient *weatherClient;
    
    PWeatherAPI *weatherAPI;
    
    NSArray *pagasaData;
    NSMutableArray *animateData;
    int imgCount;
}

@property (strong, nonatomic) IBOutlet UIImageView *msatImgView;

- (NSString *)checkMSATFolder;

@end
