//
//  PSearchController.h
//  Panahon
//
//  Created by Brian Andaliza on 8/6/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSearchControllerDelegate;

@interface PSearchController : UIView <UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *provinces;
    
    NSString *citySelected;
    
    NSArray *provincesKeys;
    NSMutableArray *currCities;
}

+ (PSearchController *)sharedInstance;

@property (weak) id <PSearchControllerDelegate> delegate;

@property (strong, nonatomic) UITableView *tblCitiesView;
@property (strong, nonatomic) UITableView *tblCitiesSubView;

@end

@protocol PSearchControllerDelegate <NSObject>

@optional

- (void)PSearchView:(PSearchController *)searchView didSelectAProvince:(NSDictionary *)searchData withCode:(NSString *)code;

- (void)PSearchView:(PSearchController *)searchView didSelectACity:(NSDictionary *)searchData;

@end
