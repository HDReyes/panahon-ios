//
//  SignupViewController.m
//  Panahon
//
//  Created by Hajji on 7/14/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "SignupViewController.h"
#import "ClassManager.h"


@interface SignupViewController ()

@end


@implementation SignupViewController
@synthesize emailTextField, passwordTextField, textFieldContainer, textScrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome-bg.png"]]];
    [[self navigationController] setNavigationBarHidden:YES];
    [[self navigationController] setToolbarHidden:YES animated:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    self.slideOutAnimationEnabled = YES;
    
    
    [self initHideKeyboard];
    //[self registerForKeyboardNotifications];
    
}

//- (IBAction)loginBtnClick:(id)sender{
//    NSLog(@"asdasdasd");
//    // Validate Login Here
//    
//    [self pushMyNewViewController];
//}
//
//- (void)pushMyNewViewController
//{
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//    UIViewController *vc ;
//    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
//    
//    // do any setup you need for myNewVC
//    
//    //[self presentViewController:myNewVC animated:YES completion:nil];
//    //[self performSegueWithIdentifier:@"HomeViewController" sender:nil];
//    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
//                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
//                                                                     andCompletion:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fbSignUpBtnClick:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"%@", result);
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    
                    [[ClassManager sharedInstance] registerAPI];
                    
                    // User is logged in, do work such as go to next view controller.
                    //                    NSLog(@"Hi Am Logged-in 3");
                    //                    [[NSNotificationCenter defaultCenter] addObserverForName:FBSDKProfileDidChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
                    //                        //do whatever you want
                    //
                    //
                    //                        NSLog(@"HHHHHHH");
                    //                    }];
                    
                }else{
                    NSLog(@"Not connected! 3");
                }
            }
        }
    }];
}

- (IBAction)signUpBtnClick:(id)sender
{
    
    NSLog(@"PASSWORD: %@ - EMAIL: %@", passwordTextField.text, emailTextField.text);
    
    NSString *email = [NSString stringWithFormat:@"%@", emailTextField.text];
    NSString *password = [NSString stringWithFormat:@"%@", passwordTextField.text];
    
    if (![[ClassManager sharedInstance] emailRegEx:email] || [email isEqualToString:@""]) {
            [self alertAction:@"Hi, you have entered an invalid E-mail address!"];
    }else{
        if ([password isEqualToString:@""]) {
            [self alertAction:@"Hi, please nominate a password!"];
        }else{
            NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", nil];
            [[ClassManager sharedInstance] validateEmail:details];
        }
    }
    
}

- (void)alertAction:(NSString *)description
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil message:description delegate:Nil cancelButtonTitle:@"Re-try" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)initHideKeyboard
{
    //Initialize Keyboard Dismiss
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tap];
    
    emailTextField.delegate = self;
    passwordTextField.delegate = self;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 301) {
        [self signUpBtnClick:textField];
        [textField resignFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)dismissKeyboard:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}


- (void)registerForKeyboardNotifications
{
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWasShown:)
    //                                                 name:UIKeyboardDidShowNotification object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillBeHidden:)
    //                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    int deviceSize = self.view.frame.size.height;
    int position;
    switch (deviceSize) {
        case 667:
            // iPhone 6
            position = 200;
            break;
        case 568:
            // iPhone 5s
            position = 190;
            break;
        case 736:
            // iPhone 6+
            position = 280;
            break;
        default:
            // iPhone 6 Default
            position = 200;
            break;
    }
    CGPoint scrollPoint = CGPointMake(0, textScrollView.frame.size.height - position);
    [textScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textScrollView setContentOffset:CGPointZero animated:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    // I'll try to make my text field 20 pixels above the top of the keyboard
    // To do this first we need to find out where the keyboard will be.
    NSValue *keyboardEndFrameValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [keyboardEndFrameValue CGRectValue];
    
    // When we move the textField up, we want to match the animation duration and curve that
    // the keyboard displays. So we get those values out now
    
    NSNumber *animationDurationNumber = [[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = [animationDurationNumber doubleValue];
    
    NSNumber *animationCurveNumber = [[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = [animationCurveNumber intValue];
    
    // UIView's block-based animation methods anticipate not a UIVieAnimationCurve but a UIViewAnimationOptions.
    // We shift it according to the docs to get this curve.
    
    
    UIViewAnimationOptions animationOptions = animationCurve << 16;
    
    // Now we set up our animation block.
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationOptions
                     animations:^{
                         // Now we just animate the text field up an amount according to the keyboard's height,
                         // as we mentioned above.
                         CGRect textFieldFrame = self.textFieldContainer.frame;
                         textFieldFrame.origin.y = keyboardEndFrame.size.height; //I don't think the keyboard takes into account the status bar
                         self.textFieldContainer.frame = textFieldFrame;
                         
                         
                         
                         //Pushes the scrollView to last row
                         NSDictionary* info = [notification userInfo];
                         CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
                         
                         
                         // If active text field is hidden by keyboard, scroll it so it's visible
                         // Your app might not need or want this behavior.
                         CGRect aRect = self.view.frame;
                         aRect.size.height -= kbSize.height;
                         if (!CGRectContainsPoint(aRect, textFieldContainer.frame.origin) ) {
                         }
                         
                         
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSNumber *animationDurationNumber = [[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = [animationDurationNumber doubleValue];
    
    NSNumber *animationCurveNumber = [[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = [animationCurveNumber intValue];
    UIViewAnimationOptions animationOptions = animationCurve << 16;
    
    NSValue *keyboardEndFrameValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [keyboardEndFrameValue CGRectValue];
    
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationOptions
                     animations:^{
                         self.textFieldContainer.frame = CGRectMake(textFieldContainer.frame.origin.x, keyboardEndFrame.size.height + textFieldContainer.frame.size.height, self.textFieldContainer.frame.size.width, self.textFieldContainer.frame.size.height); //just some hard coded value
                         

                         
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
