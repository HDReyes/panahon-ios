//
//  PeopleViewController.h
//  Panahon
//
//  Created by Hajji on 7/14/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface PeopleViewController : UIViewController<SlideNavigationControllerDelegate>

@end
