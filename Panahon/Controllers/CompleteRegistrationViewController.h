//
//  CompleteRegistrationViewController.h
//  Panahon
//
//  Created by Hajji on 7/22/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface CompleteRegistrationViewController : UIViewController<SlideNavigationControllerDelegate>

@end
