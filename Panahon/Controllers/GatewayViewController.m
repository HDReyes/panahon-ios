//
//  GatewayViewController.m
//  Panahon
//
//  Created by Hajji on 7/24/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "GatewayViewController.h"

@interface GatewayViewController ()

@end

@implementation GatewayViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"welcome-bg.png"]]];

    
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        
        // returnUserDetails Shared instance will call another graph fetch to ensure user details
        // This shared instance also loads the segue Please see this shared instance to navigate
        
        [[ClassManager sharedInstance] returnUserDetails];
        /** SEE ClassManager - Singleton SharedInstance **/
        NSLog(@"%@", [FBSDKProfile currentProfile].userID);
        
        NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:[FBSDKProfile currentProfile].userID, @"facebook_id", nil];
        
        //[[ClassManager sharedInstance] fetchLoginDetails:details];
        // Get userID using FBSDKProfile currentProfile and validate
        //1450659385258002
        //1450659385258002
        
        NSLog(@"FB Session Exist - GatewayViewController! --");

    }else{
        NSLog(@"No FB Token - GatewayViewController! --");
        
//        NSString *email     = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
//        NSString *password  = [[NSUserDefaults standardUserDefaults] stringForKey:@"password"];
        
        NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
        NSString *email = [data objectForKey:@"email"];
        NSString *password = [data objectForKey:@"password"];
        
        NSLog(@"Email Default: %@ -- Password Default: %@", email, password);
        if (email != nil || password != nil) {
            NSDictionary *details = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", nil];
            [[ClassManager sharedInstance] fetchLoginDetails:details];
            
        }
        
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES];
    [[self navigationController] setToolbarHidden:YES animated:NO];
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadSegue:(NSString *)code responseDesc:(NSString *)response{
    
    //  Code Description:
    //  1. 701 - No Email
    //  2. 702 - No Facebook Account Fetch
    //  3. 703 - Account Already Registered
    //  4. 704 - Account is clear for registration
    //  5. 705 - DB Insert Error
    //  6. 706 - Successfully registered with Facebook without E-mail
    //	7. 707 - Successfully registered with E-mail go Go Verify
    //
    //  1. 801 - Network Error
    
    // Initiates the Main story board and initiates the controller to load
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController *viewToLoad;
    
    // Sets the default condition to false to avoid loading SlideNavigation
    BOOL toChangeView = FALSE;
    
    //NSString *description;
    
    // Checks the err code the systems throw and do necessary action
    int codeInt = [code intValue];
    
    switch (codeInt) {
        case 703:
            //description = @"You need to log-in your Facebook Account";
            viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
            toChangeView = TRUE;
            break;
        case 706:
            NSLog(@"Complete Registration");
            viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"CompleteRegViewController"];
            toChangeView = TRUE;
            break;
        case 707:
            NSLog(@"Verify Code");
//            viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"CompleteRegViewController"];
//            toChangeView = TRUE;
            viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
            toChangeView = TRUE;
            
            break;
        case 708:
            NSLog(@"Route to FB Connect");
            [self fbConnect];
            //viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"CompleteRegViewController"];
            //toChangeView = TRUE;
            break;
            
        default:
            [self alertAction:response];
            break;
    }
    
    
    // do any setup you need for myNewVC
    
    //[self presentViewController:myNewVC animated:YES completion:nil];
    //[self performSegueWithIdentifier:@"HomeViewController" sender:nil];
    if (toChangeView) {
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:viewToLoad
                                                                 withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                         andCompletion:nil];
    }
    
}

- (void)fbConnect
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"%@", result);
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    
                    [[ClassManager sharedInstance] registerAPI];
                    
                }else{
                    NSLog(@"Not connected! 3");
                }
            }
        }
    }];
}

- (void)alertAction:(NSString *)description{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:description delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
