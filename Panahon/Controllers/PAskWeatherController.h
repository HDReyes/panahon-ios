//
//  PAskWeatherController.h
//  Panahon
//
//  Created by Brian Andaliza on 8/6/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "PSearchController.h"
#import "PHttpClient.h"
#import "ClassManager.h"

@interface PAskWeatherController : UIViewController <PSearchControllerDelegate, UITextFieldDelegate, PHttpClientDelegate>
{
    PHttpClient *weatherClient;
}

// modal
@property (nonatomic) IBOutlet UIView *modalView;
@property (nonatomic) IBOutlet UILabel *confirmLbl;

@property (nonatomic) NSDictionary *userCity;
@property (nonatomic) NSDictionary *userProvince;
@property (nonatomic) NSString *userLocCode;

@property (nonatomic) IBOutlet UIButton *closeSearchBtn;
@property (strong, nonatomic) IBOutlet PSearchController *searchCityView;
@property (strong, nonatomic) IBOutlet UITextField *askTxtField;
@property (strong, nonatomic) IBOutlet UIButton *submitAskBtn;

- (IBAction)submitBtnPressed;
- (IBAction)hideSearchParams;
- (IBAction)hideConfModal;

@end
