//
//  SignupViewController.h
//  Panahon
//
//  Created by Hajji on 7/14/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SlideNavigationController.h"
#import "GatewayViewController.h"

@interface SignupViewController : UIViewController<GatewayViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIButton *signUpBtn;
@property (nonatomic, strong) IBOutlet UITextField *emailTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UIView *textFieldContainer;
@property (nonatomic, strong) IBOutlet UIScrollView *textScrollView;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

- (IBAction)fbSignUpBtnClick:(id)sender;
- (IBAction)signUpBtnClick:(id)sender;

@end
