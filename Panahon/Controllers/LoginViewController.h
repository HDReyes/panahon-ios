//
//  LoginViewController.h
//  Panahon
//
//  Created by Hajji on 7/9/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//
#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SlideNavigationController.h"


@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    
}


@property (nonatomic, strong) IBOutlet UIButton *fbLoginBtn;
//@property (nonatomic, strong) IBOutlet UIButton *loginBtn;

@property (nonatomic, strong) IBOutlet UITextField *emailTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UIScrollView *textScrollView;


@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

- (IBAction)fbLoginBtnClick:(id)sender;
- (IBAction)loginBtnClick:(id)sender;


@end

//@protocol LoginViewControllerDelegate<NSObject>
//
//- (void)loadSegue;
//
//@end


