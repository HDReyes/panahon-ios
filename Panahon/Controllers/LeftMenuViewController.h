//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "ClassManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKCoreKit/FBSDKProfilePictureView.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "HomeViewController.h"

@interface LeftMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>
{
    HomeViewController *home;
    
    UIStoryboard *main;
}
@property (nonatomic, strong) NSArray *menu;
@property (nonatomic, strong) NSArray *storyBoardName;

// Properties for profile
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UILabel *firstname;
@property (nonatomic, strong) IBOutlet UILabel *email;
@property (nonatomic, strong) IBOutlet UIView *profileContainer;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (nonatomic, strong) UIImageView *icon;

@end
