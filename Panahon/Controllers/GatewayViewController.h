//
//  GatewayViewController.h
//  Panahon
//
//  Created by Hajji on 7/24/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ClassManager.h"

@class GatewayViewController;

@protocol GatewayViewControllerDelegate


@end

@interface GatewayViewController : UIViewController
{

}
// define delegate property
@property (nonatomic, assign) id  delegate;

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

- (void)loadSegue:(NSString *)code responseDesc:(NSString *)response;

@end
