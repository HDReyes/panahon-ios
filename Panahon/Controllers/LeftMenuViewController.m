//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "AppDelegate.h"

@implementation LeftMenuViewController
{
    FBSDKProfilePictureView *pictureView;
}
@synthesize icon, menu, email, profileContainer, storyBoardName, firstname;

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad 
{
	[super viewDidLoad];
    // Initialize Side Menu Name, Icon and StoryBoard Name
    [self initMenuAndStoryBoard];

    // Initialize User Profile Details on the Menu Header
    [self initUserProfileHeader];
    
	self.tableView.separatorColor = [UIColor lightGrayColor];
	
//	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
//	self.tableView.backgroundView = imageView;
   // NSLog(@"Left Menu: %@", [[ClassManager sharedInstance] profileDetails]);
    
}

- (void)initMenuAndStoryBoard{
    if ([[[ClassManager sharedInstance] profileDetails] count] > 0) {
        menu = [[NSArray alloc] initWithObjects:
                [[NSMutableDictionary alloc] initWithObjects:@[@"Home", @"home-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"PAGASA Satellite Image", @"earth-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"Tell Friends", @"tell-friends-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"My Account", @"profile-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"Logout", @"logout-icon.png"] forKeys:@[@"menu", @"icon"]],
                nil];
        storyBoardName = [[NSArray alloc] initWithObjects:@"HomeViewController",
                          @"PagasaViewController",
                          @"PeopleViewController",
                          @"ProfileViewController",
                          @"LogoutViewController",
                          nil];
    }else{
        menu = [[NSArray alloc] initWithObjects:
                [[NSMutableDictionary alloc] initWithObjects:@[@"Home", @"home-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"PAGASA Satellite Image", @"earth-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"Sign In", @"logout-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"Join Now", @"join-icon.png"] forKeys:@[@"menu", @"icon"]],
                [[NSMutableDictionary alloc] initWithObjects:@[@"Tell Friends", @"tell-friends-icon.png"] forKeys:@[@"menu", @"icon"]],
                nil];
        storyBoardName = [[NSArray alloc] initWithObjects:@"HomeViewController",
                          @"PagasaViewController",
                          @"LoginViewController",
                          @"SignupViewController",
                          @"PeopleViewController",
                          nil];
    }
}

- (void)initUserProfileHeader
{
    // Set Label Name and Email
    [firstname setText:[[[ClassManager sharedInstance] profileDetails] objectForKey:@"firstname"]];
    [email setText:[[[ClassManager sharedInstance] profileDetails] objectForKey:@"email"]];
    
    NSLog(@"FACEBOOK ID: %lu", (unsigned long)[[[ClassManager sharedInstance] profileDetails] count]);
//    
//    if ([[[ClassManager sharedInstance] profileDetails] count] ) {
//        NSLog(@"null");
//    }
    
    // Checks if facebook ID Exist/ If not dont make Profile Photo
    if ([[[ClassManager sharedInstance] profileDetails] count] > 0) {
        
        if (!([[[[ClassManager sharedInstance] profileDetails] objectForKey:@"facebook_id"] isEqualToString:@"0"]))
        {
            
            NSLog(@"hello");
            // Initialize Facebook Profile Picture and Round
            
            pictureView=[[FBSDKProfilePictureView alloc] initWithFrame:CGRectMake(15, (firstname.frame.origin.x + firstname.frame.size.height) + 15, 70, 70)];
            
            [pictureView setProfileID:[[[ClassManager sharedInstance] profileDetails] objectForKey:@"facebook_id"]];
            [pictureView setPictureMode:FBSDKProfilePictureModeSquare];
            [pictureView layer].cornerRadius = 35.0f;
            [pictureView layer].masksToBounds = YES;
            //[pictureView layer].borderWidth = 3;
            //[pictureView layer].borderColor = [[UIColor colorWithRed:0.200 green:0.400 blue:0.800 alpha:0.500] CGColor];
            [self.view addSubview:pictureView];
            
            
        }
        
    }
    
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [menu count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
	view.backgroundColor = [UIColor clearColor];
	return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuCell"];
	cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //[cell.textLabel setTextColor:[UIColor whiteColor]];
    
    // Adjusts the icon position on smaller iOS device
//    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
//        icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 8, 30, 30)];
//    }else{
//    }
//    icon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 16, 14, 14)];
	
	cell.backgroundColor = [UIColor clearColor];
	
    
    
    // Set up the cell...
    //[cell addSubview: [icon initWithImage:[UIImage imageNamed:[menu[indexPath.row] objectForKey:@"icon"]]]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [menu[indexPath.row] objectForKey:@"menu"]];
    cell.imageView.image = [UIImage imageNamed:[menu[indexPath.row] objectForKey:@"icon"]];
    //cell.textLabel.numberOfLines = 1;
    
    
    
    
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    if ([[[menu objectAtIndex:indexPath.row] objectForKey:@"menu"] isEqualToString:@"Logout"]) {
        [self logout];
        
    }else{
        
        // capture instance of home
        if([[[SlideNavigationController sharedInstance].viewControllers objectAtIndex:1] isKindOfClass:[HomeViewController class]])
        {

            home = [[SlideNavigationController sharedInstance].viewControllers objectAtIndex:1];
        }
        
        UIViewController *vc;
        
        if([[storyBoardName objectAtIndex:indexPath.row] isEqualToString:@"HomeViewController"])
        {
            vc = home;
        }else
        {
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:[storyBoardName objectAtIndex:indexPath.row]];
        }
        
        [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                                withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                         andCompletion:nil];
        
        
    }
    
    //NSLog(@"mango");
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //Sets the cell BG according with the Table BG
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    // change background color of selected cell
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    [myBackView setBackgroundColor:[UIColor colorWithWhite:1.000 alpha:0.000]];
    [myBackView setAlpha:0];
    [myBackView setOpaque:NO];
    cell.selectedBackgroundView = myBackView;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *sampleView = [[UIView alloc] init];
    sampleView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
    
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 20, 150, 50)];
    footerLabel.text = @"Terms and Conditions";
    [footerLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    footerLabel.textColor = [UIColor grayColor];
    
    UIView *bottomBorder = [UIView new];
    bottomBorder.backgroundColor = [UIColor lightGrayColor];
    [bottomBorder setAlpha:0.5];
    bottomBorder.frame = CGRectMake(0, 30, sampleView.frame.size.width, 1);
    
    [sampleView addSubview:bottomBorder];
    
    [sampleView addSubview:footerLabel];
    return sampleView;
}

- (void)logout
{
    UIActionSheet *logoutSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure, you want to Logout?"
                                                             delegate:self cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Logout"
                                                    otherButtonTitles:nil, nil, nil];
    [logoutSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            if (TRUE) {
                // if a user logs out explicitly, we delete any cached token information, and next
                // time they run the applicaiton they will be presented with log in UX again; most
                // users will simply close the app or switch away, without logging out; this will
                // cause the implicit cached-token login to occur on next launch of the application
                //[[FBSession activeSession] closeAndClearTokenInformation];
                //NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                //[[SMLClassManager sharedInstance] storeUserInformation:nil];
//                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"myapp.sessionID"];
//                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"email"];
//                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"password"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"Yes 1");
                [self logOut];
            }
            break;
        case 1:
            NSLog(@"Yes 2");
            
            break;
        case 2:
            NSLog(@"Yes 3");
            
            break;
        default:
            
            break;
    }
}


- (void)logOut
{
    //Clears out the NSUserDefault Details
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"password"];
    
    //Synchronize NSUserDefaults
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Clears out Facebook Session
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    // Clears out Facebook Token and Profile
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    
    // Clears the profileDetails variable
    [[ClassManager sharedInstance] clearDetails];
    
    // Re-initialize profile header
    [self initUserProfileHeader];
    
    // Removes the user Image from view
    [pictureView removeFromSuperview];
    
    // Re-initialize menu
    [self initMenuAndStoryBoard];
    
    // Reload table view
    [self.tableView reloadData];
    
    // Initiates the Main story board and initiates the controller to load
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController *viewToLoad = [mainStoryboard instantiateViewControllerWithIdentifier: @"GatewayViewController"];
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:viewToLoad
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
    //[[[UIApplication sharedApplication] delegate] performSelector:@selector(reloadApp)];
    
}



@end
