//
//  PAskWeatherController.m
//  Panahon
//
//  Created by Brian Andaliza on 8/6/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "PAskWeatherController.h"
#import "PConfig.h"

@interface PAskWeatherController ()

@end

@implementation PAskWeatherController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.searchCityView.delegate = self;
    
    weatherClient           = [PHttpClient new];
    weatherClient.delegate  = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.askTxtField.text = [NSString stringWithFormat:@"%@ / %@", [self.userProvince valueForKey:@"province"], [self.userCity valueForKey:@"name"]];
}

- (void)hideSearchParams
{
    self.closeSearchBtn.hidden = YES;
    self.searchCityView.hidden = YES;
}

- (void)submitBtnPressed
{
    self.submitAskBtn.enabled = NO;
    
    NSDictionary *profileInfo = [[ClassManager sharedInstance] profileDetails];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              PANAHON_API_KEY, @"api_key",
                              [self.userProvince valueForKey:@"province"], @"province",
                              [self.userCity valueForKey:@"name"], @"city",
                              [profileInfo valueForKey:@"userid"], @"userid",
                              nil];
    
    [weatherClient connectToURL:PANAHON_ASK_WEATHER_URL withData:postData postType:@"post"];
}

- (void)hideConfModal
{
    self.modalView.hidden = YES;
}

- (void)dispConfModal:(NSDictionary *)data
{
    self.modalView.hidden = NO;
    
    self.confirmLbl.text = [NSString stringWithFormat:@"%@", [data valueForKey:@"response"]];
}

#pragma mark - PHttpClient Delegates
- (void)PHttpClient:(PHttpClient *)httpClient connectionDataWasRetrieved:(id)data
{
    self.submitAskBtn.enabled = YES;
    
    [self dispConfModal:data];
}

#pragma mark - UITextField Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.searchCityView.hidden = NO;
    self.closeSearchBtn.hidden = NO;
    
    return NO;
}

#pragma mark - PSearchController Delegates
- (void)PSearchView:(PSearchController *)searchView didSelectACity:(NSDictionary *)searchData
{
    self.userCity = searchData;
    
    [self hideSearchParams];
    
    self.askTxtField.text = [NSString stringWithFormat:@"%@ / %@", [self.userProvince valueForKey:@"province"], [self.userCity valueForKey:@"name"]];
}

- (void)PSearchView:(PSearchController *)searchView didSelectAProvince:(NSDictionary *)searchData withCode:(NSString *)code
{
    self.userLocCode = code;
    
    self.userProvince = searchData;
}



@end
