//
//  ProfileViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface ProfileViewController : UIViewController <SlideNavigationControllerDelegate>

@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (nonatomic, assign) IBOutlet UITextField *email;
@property (nonatomic, assign) IBOutlet UITextField *oldPassword;
@property (nonatomic, assign) IBOutlet UITextField *nPassword;
@property (nonatomic, assign) IBOutlet UITextField *confirmPass;


- (IBAction)changePassword:(id)sender;
- (IBAction)facebookConnect:(id)sender;

@end
