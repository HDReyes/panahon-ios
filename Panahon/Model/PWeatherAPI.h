//
//  PWeatherAPI.h
//  Panahon
//
//  Created by Brian Andaliza on 7/10/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PHttpClient.h"

@protocol PWeatherAPIDelegate;

@interface PWeatherAPI : NSObject <PHttpClientDelegate>
{
    PHttpClient *weatherClient;
}

@property (weak) id <PWeatherAPIDelegate> delegate;

@property (strong, nonatomic) PWeatherAPI *weatherAPI;

+ (PWeatherAPI *)sharedInstance;

- (void)retrieveWeatherByCity:(NSString *)code;
- (void)retrieveMSATImages;

- (UIImage*)downloadMSATImage:(NSString*)url;
- (void)saveMSATImage:(UIImage *)image filename:(NSString *)filename;
- (UIImage *)getMSATImage:(NSString *)filename;
@end

@protocol PWeatherAPIDelegate <NSObject>

@optional

- (void)PWeatherAPI:(PWeatherAPI *)weatherAPI didRetrieveWeatherByCity:(NSDictionary *)weatherData;

- (void)PWeatherAPI:(PWeatherAPI *)weatherAPI didRetrieveMSATImages:(NSArray *)wImages;

@end