//
//  ClassManager.h
//  Panahon
//
//  Created by Hajji on 7/16/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassManager : NSObject
{
    //NSDictionary *profileDetails;
}
@property (nonatomic, retain) NSDictionary *profileDetails;
+ (ClassManager*)sharedInstance;

//API for PROFILE: fetch, add, delete

- (BOOL)registerAPI;
- (void)validateAccount;

// Validate E-mail registration
- (void)validateEmail:(NSDictionary *)details;

// Returns User Info
- (void)returnUserDetails;

// Validates corect E-mail
- (BOOL)emailRegEx:(NSString *)email;


- (void)fetchLoginDetails:(NSDictionary *)details;

- (void)clearDetails;


@end
