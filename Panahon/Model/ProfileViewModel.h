//
//  ProfileViewModel.h
//  Panahon
//
//  Created by Hajji on 7/16/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AFNetworking.h"
#import "GatewayViewController.h"
#import "ClassManager.h"

@interface ProfileViewModel : NSObject<GatewayViewControllerDelegate>

- (BOOL)registerToAPI;
- (void)validateAccount:(void (^)(BOOL isLoggedIn, NSString *response, NSString *errcode))callback;
- (void)fetchGraphProfile:(void (^)(NSDictionary *))callback;

//- (void)fetchUserDetails;
- (void)fetchUserDetails:(void (^)(NSDictionary *))callback;
//- (NSDictionary *)storedUserDetails;

- (void)validateEmail:(NSDictionary *)details;

- (void)checkLogin:(NSDictionary *)details addCall:(void (^)(NSDictionary *))callback;
@end

