//
//  PSettings.m
//  Panahon
//
//  Created by Brian Andaliza on 7/8/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import "PSettings.h"

@implementation PSettings

// returns full directory
- (NSString *)dataFilePath
{
    // Get the full path of file in NSDocuments
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    return [paths objectAtIndex:0];
}


@end
