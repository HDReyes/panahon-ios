//
//  PWeatherType.h
//  Panahon
//
//  Created by Brian Andaliza on 8/4/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWeatherType : UIView

@property (strong, nonatomic) NSString *type;
@property (nonatomic) BOOL enabled;

@end
