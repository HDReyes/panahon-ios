//
//  SMLHttpClient.h
//  SMLMapModuleVer2
//
//  Created by Brian Andaliza on 8/18/14.
//  Copyright (c) 2014 Havoc Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHttpClientDelegate;

@interface PHttpClient : NSObject

@property (nonatomic) NSDictionary *connData;
@property (nonatomic) NSString *type;

@property (weak) id <PHttpClientDelegate> delegate;

- (void)connectToURL:(NSString *)url withData:(NSDictionary *)passedData postType:(NSString *)type;

@end


@protocol PHttpClientDelegate <NSObject>

@optional
- (void)PHttpClient:(PHttpClient *)httpClient connectionDataWasRetrieved:(id)data;

@end
