//
//  PWeatherType.m
//  Panahon
//
//  Created by Brian Andaliza on 8/4/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import "PWeatherType.h"

@implementation PWeatherType

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setEnabled:(BOOL)enabled
{
    UIImageView *bgView = [[self subviews] objectAtIndex:0];
    
    if(enabled)
    {
        bgView.image = [UIImage imageNamed:@"w-weatherBG-high.png"];
    }
    else
    {
        bgView.image = nil;
    }
}

@end
