//
//  SMLHttpClient.m
//  SMLMapModuleVer2
//
//  Created by Brian Andaliza on 8/18/14.
//  Copyright (c) 2014 Havoc Digital. All rights reserved.
//


#import "PHttpClient.h"
#import "AFNetworking.h"

@implementation PHttpClient

- (void)connectToURL:(NSString *)url withData:(NSDictionary *)passedData postType:(NSString *)type
{
    // try to connect to host
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    if([type isEqualToString:@"post"])
    {
        [manager POST:url parameters:passedData success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             // save return data
             [self.delegate PHttpClient:self connectionDataWasRetrieved:responseObject];
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             UIAlertView *avError = [[UIAlertView alloc] initWithTitle:@"Snap!"
                                                               message:@"Check your net connection, either you don't have one right now or it's just too slow."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [avError show];
             
             NSLog(@"%@", error);
         }];
    }
    else
    {
        [manager GET:url parameters:passedData success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             // save return data
             [self.delegate PHttpClient:self connectionDataWasRetrieved:responseObject];
         }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             UIAlertView *avError = [[UIAlertView alloc] initWithTitle:@"Snap!"
                                                           message:@"Check your net connection, either you don't have one right now or it's just too slow."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil];
         
             [avError show];

             NSLog(@"%@", error);
         }];
    }
}

@end


