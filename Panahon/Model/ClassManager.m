//
//  ClassManager.m
//  Panahon
//
//  Created by Hajji on 7/16/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import "ClassManager.h"
#import "ProfileViewModel.h"

@interface ClassManager()
{
    ProfileViewModel *profileModel;
    
}

@end

@implementation ClassManager

@synthesize profileDetails;

+ (ClassManager*)sharedInstance
{
    // 1
    static ClassManager *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ClassManager alloc] init];
    });
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        profileModel         = [[ProfileViewModel alloc] init];
    }
    return self;
}

- (BOOL)registerAPI{
    
    return [profileModel registerToAPI];
}

- (void)validateAccount
{
    //return [profileModel validateAccount:(void (^)(BOOL isLoggedIn, NSString *response, NSString *errcode))];
    [profileModel validateAccount:^(BOOL isLoggedIn, NSString *response, NSString *errcode){
        //NSLog(@"}}}}}}%d====%@======>%@", isLoggedIn, response, errcode);
        
    }];

}

- (void)clearDetails{
    profileDetails = [[NSDictionary alloc] init];
     NSLog(@"Supposed to be clear! %@", profileDetails);
}

- (void)returnUserDetails
{
    //[profileModel fetchUserDetails];
    [profileModel fetchUserDetails:^(NSDictionary *result) {
        NSLog(@"Returns user details! --------------");
        profileDetails = result;
        // Load the Home segue, see ProfileViewModel for code reference
        //NSLog(@"&&&&&&%@", profileDetails);
    }];
}

- (void)fetchLoginDetails:(NSDictionary *)details{
    [profileModel checkLogin:details addCall:^(NSDictionary *result) {
        profileDetails = result;
    }];
}


//- (void)checkLogin:(NSDictionary *)details addCall:(void (^)(NSDictionary *))callback{
//
//}

- (BOOL)emailRegEx:(NSString *)email
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

- (void)validateEmail:(NSDictionary *)details
{
    [profileModel validateEmail:details];
}

@end
