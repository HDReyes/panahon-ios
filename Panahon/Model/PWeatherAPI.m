//
//  PWeatherAPI.m
//  Panahon
//
//  Created by Brian Andaliza on 7/10/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import "PWeatherAPI.h"
#import "PConfig.h"

@implementation PWeatherAPI

@synthesize weatherAPI = _weatherAPI;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        weatherClient = [PHttpClient new];
        
        weatherClient.delegate = self;
    }
    
    return self;
}

+ (PWeatherAPI *)sharedInstance
{
    static PWeatherAPI *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [PWeatherAPI new];
    });
    
    return _sharedInstance;
}

- (void)retrieveMSATImages
{
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              PANAHON_API_KEY, @"api_key"
                              , nil];
    
    weatherClient.type = @"msat";
    
    [weatherClient connectToURL:PANAHON_PAGASA_IMG_URL withData:postData postType:@"get"];
}

- (void)retrieveWeatherByCity:(NSString *)code
{
    NSString *province = code;
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              PANAHON_API_KEY, @"api_key"
                              , nil];
    
    weatherClient.type = @"wthr_cities";
    
    [weatherClient connectToURL:[PANAHON_CITY_URL stringByAppendingString:province] withData:postData postType:@"get"];
}

- (UIImage*)downloadMSATImage:(NSString*)url
{
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    return [UIImage imageWithData:data];
}

- (void)saveMSATImage:(UIImage *)image filename:(NSString *)filename
{
    filename = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/MSAT/%@", filename];
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile:filename atomically:YES];
}

- (UIImage *)getMSATImage:(NSString *)filename
{
    filename = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/MSAT/%@", filename];
    NSData *data = [NSData dataWithContentsOfFile:filename];
    return [UIImage imageWithData:data];
}

#pragma mark - PHttpClient Delegates
- (void)PHttpClient:(PHttpClient *)httpClient connectionDataWasRetrieved:(id)data
{
    if([httpClient.type isEqualToString:@"wthr_cities"])
    {
        [self.delegate PWeatherAPI:self didRetrieveWeatherByCity:[data valueForKey:@"data"]];
    }else if ([httpClient.type isEqualToString:@"msat"])
    {
        [self.delegate PWeatherAPI:self didRetrieveMSATImages:[data valueForKey:@"data"]];
    }
}

@end
