//
//  PSettings.h
//  Panahon
//
//  Created by Brian Andaliza on 7/8/15.
//  Copyright (c) 2015 NuRun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSettings : NSObject

- (NSString *)dataFilePath;

@end
