//
//  ProfileViewModel.m
//  Panahon
//
//  Created by Hajji on 7/16/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//
//
//  Error Code:
//  1. 701 - No Email
//  2. 702 - No Facebook Account Fetch
//  3. 703 - Account Already Registered
//  4. 704 - Account is clear for registration
//  5. 705 - DB Insert Error
//  6. 706 - Successfully registered with Facebook without E-mail
//  7. 707 - Successfully registered with E-mail go Go Verify
//  8. 708 - Logging-in using E-mail but has already registered with Facebook Account
//
//  1. 801 - Network Error
//
//
//
//

#import "ProfileViewModel.h"

@interface ProfileViewModel()
{
    NSMutableDictionary *profileDetails;
    GatewayViewController *gatewayView;
    
}

@end

@implementation ProfileViewModel

-(id)init {
    
    self = [super init];
    
    gatewayView = [[GatewayViewController alloc] init];
    gatewayView.delegate = self;
    return self;
    
}

-(BOOL)registerToAPI{
    // Build profile info
    if ([FBSDKAccessToken currentAccessToken]) {

//        profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                          [FBSDKProfile currentProfile].userID,     @"facebook_id",
//                          [FBSDKProfile currentProfile].firstName,  @"firstname",
//                          [FBSDKProfile currentProfile].lastName,   @"lastname",
//                          [FBSDKProfile currentProfile].name,       @"fullname",
//                          [FBSDKProfile currentProfile].linkURL,    @"linkUrl",
//                          nil];
    }
    // Fetch extended persmission in the background
//    [self fetchGraphProfile:^(NSDictionary* result){
//        [profileDetails setObject:[result objectForKey:@"email"] forKey:@"email"];
//        [profileDetails setObject:[result objectForKey:@"id"] forKey:@"id"];
//    }];

    // Validate the status of account using facebook account
    [self validateAccount:^(BOOL isNotRegistered, NSString *response, NSString *errcode){
        NSLog(@"RegisterAPI Validate Account: %d====%@======>%@", isNotRegistered, response, errcode);
        
        // IF isNotRegistered is TRUE means the account is not yet on DB continue with the registration process
        if (isNotRegistered) {
            // IF Facebook account is not registered continue with registration
            [self registerAccount:profileDetails addCall:^(BOOL isLoggedIn, NSString *response, NSString *errcode){
                NSLog(@"RegisterAPI Regster Status: %d====%@======>%@", isLoggedIn, response, errcode);
                

                [gatewayView loadSegue:errcode responseDesc:response];
            }];
            
        }else{
            [gatewayView loadSegue:errcode responseDesc:response];
        }
        [[ClassManager sharedInstance] returnUserDetails];
    }];
    
    return TRUE;
}

- (void)fetchGraphProfile:(void (^)(NSDictionary *))callback {
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email,first_name,last_name,link" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  id result, NSError *error) {
         
         profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                           [result objectForKey:@"id"],         @"facebook_id",
                           [result objectForKey:@"first_name"],  @"firstname",
                           [result objectForKey:@"last_name"],   @"lastname",
                           [result objectForKey:@"name"],        @"fullname",
                           [result objectForKey:@"link"],        @"linkUrl",
                           [result objectForKey:@"email"],       @"email",
                           nil];
         
//         NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"], @"email", [result objectForKey:@"id"], @"id", nil];
         //NSLog(@"GRAPH:%@", result);
         
         callback((NSDictionary*) profileDetails);
     }];

}



- (void)validateAccount:(void (^)(BOOL isNotRegistered, NSString *response, NSString *errcode))callback
{
    // Fetch extended persmission in the background
    [self fetchGraphProfile:^(NSDictionary* result){
        
        //Checks the status of E-mail -> IF NO E-mail Found continue with Registration Set Email to Empty
        NSString *mail = @"";
        if (![[result objectForKey:@"email"] isEqualToString:@""]) {
            mail = [result objectForKey:@"email"];
        }
        
        // Passes extended permission to NSMutableDictionary profileDetails
//        [profileDetails setObject:mail forKey:@"email"];
//        [profileDetails setObject:[result objectForKey:@"id"] forKey:@"id"];
//        NSLog(@"---%@----", profileDetails);
        // Checks if the user has an Email Address
//        if ([result objectForKey:@"email"]) {
        
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            [manager POST:@"http://fbappbox.com/panahontv-api/index.php/api_call/validate_account" parameters:profileDetails success:^(AFHTTPRequestOperation *operation, id responseObject) {
                // Return Call Back Value
                BOOL status = [[responseObject objectForKey:@"status"] boolValue];
                NSString *response = [responseObject objectForKey:@"response"];
                NSString *errcode  = [responseObject objectForKey:@"errcode"];
                callback(status, response, errcode);
            
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"%@", error);

                // Return Call Back Value
                NSString *response = @"Network Error.";
                NSString *errcode  = @"801";
                callback(FALSE, response, errcode);
            }];
            
            
//        }else{
//            
//            NSString *response = @"No Email!";
//            NSString *errcode  = @"701";
//            
//            callback(FALSE, response, errcode);
//        }
        
        
    }];
    
    
    
}

- (BOOL)checkEmail:(NSString *)email{
    return TRUE;
}


- (void)registerAccount:(NSDictionary *)details addCall:(void (^)(BOOL isNotRegistered, NSString *response, NSString *errcode))callback
{
    NSLog(@"+++++%@++++", details);

        // Continue the registration
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            [manager POST:@"http://fbappbox.com/panahontv-api/index.php/api_call/register_account" parameters:details success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                BOOL status = [[responseObject objectForKey:@"status"] boolValue];
                NSString *response = [responseObject objectForKey:@"response"];
                NSString *errcode  = [responseObject objectForKey:@"errcode"];
                
                //NSLog(@"ERROR: %@", responseObject);
                
                //[test.delegate loadSegue];
                //[loginView loadSegue:errcode];
                callback(status, response, errcode);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                //NSLog(@"REG ERROR: %@", error);
                NSString *response = @"Network Error.";
                NSString *errcode  = @"801";
                
                callback(FALSE, response, errcode);
            }];
            
            
    
        
        
    
    
    
}

- (void)fetchUserDetails:(void (^)(NSDictionary *))callback
{
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email,first_name,last_name,link,picture" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                  id result, NSError *error) {
         
         if ([result count] > 0) {
             profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                               [result objectForKey:@"id"],          @"facebook_id",
                               [result objectForKey:@"first_name"],  @"firstname",
                               [result objectForKey:@"last_name"],   @"lastname",
                               [result objectForKey:@"name"],        @"fullname",
                               [result objectForKey:@"link"],        @"linkUrl",
                               [result objectForKey:@"email"],       @"email",
                               [result objectForKey:@"picture"],      @"picture",
                               // This is an additional
                               @"ios",      @"platform",
                               
                               nil];
         }else{
             NSLog(@"No details was fetch - fetchUserDetails!");
         }
         
         
         NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"], @"email", [result objectForKey:@"id"], @"facebook_id", nil];
         
         [[ClassManager sharedInstance] fetchLoginDetails:info];
         //NSLog(@"GRAPH:%@", result);
         //[gatewayView loadSegue:@"703" responseDesc:@"Success"];
         callback((NSDictionary*) profileDetails);
         
     }];
}

- (void)validateEmail:(NSDictionary *)details
{
    
    profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                      @"",                              @"facebook_id",
                      @"Guest",                         @"firstname",
                      @"",                              @"lastname",
                      @"",                              @"fullname",
                      @"",                              @"linkUrl",
                      [details objectForKey:@"email"],   @"email",
                      @"",                              @"picture",
                      [details objectForKey:@"password"],  @"password",
                      @"ios",                            @"platform",
                      nil];
        // Passes extended permission to NSMutableDictionary profileDetails
        //        [profileDetails setObject:mail forKey:@"email"];
        //        [profileDetails setObject:[result objectForKey:@"id"] forKey:@"id"];
        //        NSLog(@"---%@----", profileDetails);
        // Checks if the user has an Email Address
        //        if ([result objectForKey:@"email"]) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager POST:@"http://fbappbox.com/panahontv-api/index.php/api_call/validate_account" parameters:profileDetails success:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Return Call Back Value
            //BOOL status = [[responseObject objectForKey:@"status"] boolValue];
            NSString *response = [responseObject objectForKey:@"response"];
            NSString *errcode  = [responseObject objectForKey:@"errcode"];
            NSLog(@"%@", responseObject);
            
            
            //IF error code is 704 - it means the E-mail address is not yet registered and continue with registration
            if ([errcode isEqualToString:@"704"]) {
                [self registerAccount:profileDetails addCall:^(BOOL isLoggedIn, NSString *response, NSString *errcode){
                    NSLog(@"----}}}}}}%d====%@======>%@", isLoggedIn, response, errcode);
                    if (isLoggedIn) {
                        [self setUserDefaults:details];
                        [[ClassManager sharedInstance] fetchLoginDetails:details];
                        [gatewayView loadSegue:errcode responseDesc:response];
                    }else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:response delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    
                }];
            }else{
//                [self setUserDefaults:details];
//                [gatewayView loadSegue:errcode responseDesc:response];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:response delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@", error);
            
            // Return Call Back Value
            NSString *response = @"Network Error.";
            NSString *errcode  = @"801";
            [gatewayView loadSegue:errcode responseDesc:response];
            
        }];
        
}

- (void)checkLogin:(NSDictionary *)details addCall:(void (^)(NSDictionary *))callback
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"http://fbappbox.com/panahontv-api/index.php/api_call/check_login" parameters:details success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Return Call Back Value
        //BOOL status = [[responseObject objectForKey:@"status"] boolValue];
        NSString *response = [responseObject objectForKey:@"response"];
        NSString *errcode  = [responseObject objectForKey:@"errcode"];
        
        NSLog(@"CheckLogin: %@",responseObject);
        
        //Create the session
        //Encode the email address and datetime to base64
        
        
        if ([[responseObject objectForKey:@"user_info"] count] > 0) {
            for (id userInfo in [responseObject objectForKey:@"user_info"]){
                NSLog(@"%@", [userInfo objectForKey:@"email"]);
                profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                  [userInfo objectForKey:@"userid"],                @"userid",
                                  [userInfo objectForKey:@"facebook_id"],           @"facebook_id",
                                  [userInfo objectForKey:@"firstname"],             @"firstname",
                                  [userInfo objectForKey:@"lastname"],              @"lastname",
                                  [userInfo objectForKey:@"fullname"],              @"fullname",
                                  [userInfo objectForKey:@"link_url"],              @"linkUrl",
                                  [userInfo objectForKey:@"email"],                @"email",
                                  [userInfo objectForKey:@"datetime_registered"],   @"datetime_registered",
                                  [userInfo objectForKey:@"account_status"],        @"account_status",
                                  nil];
            }
            [self setUserDefaults:details];
        }
        
//        profileDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                          [result objectForKey:@"id"],         @"facebook_id",
//                          [result objectForKey:@"first_name"],  @"firstname",
//                          [result objectForKey:@"last_name"],   @"lastname",
//                          [result objectForKey:@"name"],        @"fullname",
//                          [result objectForKey:@"link"],        @"linkUrl",
//                          [result objectForKey:@"email"],       @"email",
//                          [result objectForKey:@"picture"],       @"picture",
//                          
//                          nil];
//        
//        
//        //         NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"], @"email", [result objectForKey:@"id"], @"id", nil];
//        //NSLog(@"GRAPH:%@", result);
        
        callback((NSDictionary*) profileDetails);
        [gatewayView loadSegue:errcode responseDesc:response];
        
        //[gatewayView loadSegue:errcode responseDesc:response];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"===>%@", error);
        
        // Return Call Back Value
        NSString *response = @"Network Error.";
        NSString *errcode  = @"801";
        [gatewayView loadSegue:errcode responseDesc:response];
        
    }];

}


- (void)setUserDefaults:(NSDictionary *)details{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyyMMddHH"];
//    NSString *strdate = [NSString stringWithFormat:@"%@-%@",[details objectForKey:@"email"], [dateFormatter stringFromDate:[NSDate date]]]; //provide your selected date here
//    
//    [[NSUserDefaults standardUserDefaults] setObject:[[strdate dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0] forKey:@"myapp.sessionID"];
//    [details objectForKey:@"email"];
    
    //Fill-out the NSUserDefault Details
    [[NSUserDefaults standardUserDefaults] setValue:[details objectForKey:@"email"] forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:[details objectForKey:@"password"] forKey:@"password"];
    //Synchronize NSUserDefaults
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//-(void)isLoggedIn:(void(^)(BOOL isLoggedIn))completionBlock {
//    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", kBaseURL, kAPI, kUserIsLogged];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//        completionBlock(YES);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//        completionBlock(NO);
//    }];
//}
@end
