//
//  AppDelegate.h
//  Panahon
//
//  Created by Hajji on 7/8/15.
//  Copyright (c) 2015 Publicis-Manila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "LoginViewController.h"
#import "AFNetworking.h"
#import "HomeViewController.h"
@class Reachability;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability *internetReach;
}
@property (strong, nonatomic) UIWindow *window;

- (void)reloadApp;

@end

